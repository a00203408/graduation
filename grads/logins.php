<?php 
session_start();

if(isset($_POST['submit'])) 
{

	$_SESSION['fac'] = $_POST['fac'];
	$_SESSION['degree'] = $_POST['degree'];
	$_SESSION['course'] = $_POST['course'];
	$_SESSION['year'] = $_POST['year'];
}
?>


<!DOCTYPE html>
<html>
	<head>
		<title>Graduates Registration</title>
		<link rel="stylesheet" href="main.css">
		<script src="loginn.js"></script>
		
	</head>
	<body>	  
		<ul id="m">
			<img src="aitt.png" class="nav" id="ait">
			<li class="nav"><a href="https://www.ait.ie">AIT Home page</a></li>
			<li class="nav"><a href="home.html">Home</a></li>
			<li class="nav"><a href="indexx.html">Staff Login</a></li>
			<li class="nav"><a href="http://timetable.ait.ie/students.htm">Student Timetable</a></li>
			<li class="dropdown">
			    <a href="https://www.ait.ie/life-at-ait/registry/graduation" class="dropbtn">Graduation</a>
				<div class="dropdown-content">
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Timetable</a>
					<a href="https://www.lafayette.ie ">Graduation Photography</a>
					<a href="https://www.phelanconan.com/">Academic Dress</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Ball</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Assembly Point</a>		
			<li class="nav"><a href="location.html">Graduation Location</a></li>
			<li class="nav"><a href="contact.html">Contact us</a></li>
			<li class="nav"><a href="Data.php">Data</a></li>
			<li class="nav"><?php echo "<a href='logoutxx.php'>Log out</a>";?></li>
		</ul>

		<body background="kkkk2.jpg"/>
		
		<div class="container">
		<div class="s">
			<form action="" method="post">
				<img src="login10.png" id="logos"/><h2 id="inf">Staff List </h2>
				<ul>
					<li>
					  <label for="fac">Faculty :</label>
					  <select id="fac" name="fac" required>
					    <option value="">Select course Faculty ..</option>
						<option value="Business">Business</option>
						<option value="Engineering">Engineering</option>
						<option value="pharmaceutical">pharmaceutical</option>
						<option value="Sport">Sport </option>
						<option value="Science">Science </option>
					  </select>	
					</li>
					<li>
						<label for="degree">Degree :</label>
					  <select id="degree" name="degree" required>
					    <option value="">Select Course Degree..</option>
						<option value="Associate degree">Associate degree</option>
						<option value="Bachelor degree">Bachelor degree</option>
						<option value="Master degree">Master degree	</option>
						<option value="Doctoral degree">Doctoral degree	</option>
					  </select>	
					</li>
					<li>
						<label for="course">Required Course :</label>
						<input type="text" name="course" id="course" class="text" placeholder="Enter Course Name" required>
						
					</li>
					
					<li>
						<label for="course">Academic Year :</label>
						<input type="text" name="year" id="year" class="text" placeholder="ie: 2017" required>
						
					</li>
					
					<li>
						<label for="submit">&nbsp;</label>
						<input type="submit" id="submit" name="submit" value="Student List">
					</li>
				</ul>
			</form>	

		</div>	
		</div>
	</body>
	<footer>
		<div class="foot">
		<p>Get social with us..
		<a href="https://twitter.com/AthloneIT?lang=en"><img src="t.gif" class="foo"></a>
		<a href="https://www.facebook.com/athloneit/"><img src="f.gif" class="foo"></a>
		<a href="https://www.linkedin.com/school/860289/"><img src="i.gif" class="foo"></a></p>
		
		</div>
	</footer>

</html>