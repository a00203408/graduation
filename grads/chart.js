		window.onload = function () {
		
		var ctx = document.getElementById("myCharts").getContext('2d');
			var myChart = new Chart(ctx, {	
			//pie if it doesn't work/ polarArea
				  type: 'pie',
				  data: {
					labels: ["Business", "Engineering", "Science"],
					datasets: [{
					  backgroundColor: [
						"#ff9933",
						"#666699",
						"#99003d"
					  ],
					  data: [<?php echo $bu; ?>,<?php echo $en; ?>,<?php echo $sc; ?>]
					  //data: [100, 300,500]
					}]
				  },
				  options: {
					title: {
					display: true,
					text: 'Attendance of students by dominates majors'
					}
				  }
				});
		}