<!DOCTYPE html>
<html>
	<head>
		<title>Graduates Registration</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="main.css">
		<script src="signin.js"></script>
		
	</head>
	<body>	  
		<ul id="m">
			<img src="aitt.png" class="nav" id="ait">
			<li class="nav"><a href="home.html">Home</a></li>
			<li class="nav"><a href="https://www.ait.ie">AIT Home page</a></li>
			<li class="nav"><a href="indexx.html">Staff Login</a></li>
			<li class="nav"><a href="http://timetable.ait.ie/students.htm">Student Timetable</a></li>
			<li class="dropdown">
			    <a href="https://www.ait.ie/life-at-ait/registry/graduation" class="dropbtn">Graduation</a>
				<div class="dropdown-content">
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Timetable</a>
					<a href="https://www.lafayette.ie ">Graduation Photography</a>
					<a href="https://www.phelanconan.com/">Academic Dress</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Ball</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Assembly Point</a>		
			<li class="nav"><a href="location.html">Graduation Location</a></li>
			<li class="nav"><a href="contact.html">Contact us</a></li>
			<li class="nav"><?php echo "<a href='logoutx.php'>Log out</a>";?></li>
		</ul>

		<body background="kkk2.jpg"/>
		
		<div class="container">
		<div class="s">

			<form id="myForm" name="students" action="detail.php" method="post">
				<img src="login10.png" id="logos"/><h2 id="inf">Student Information </h2>
				<ul>
					
					<li>
						<label for="name">Student Name :</label>
						<input type="text" id="name" name="name" class="text" placeholder="Enter your Full Name" required>
						
					</li>
					<li>
					  <label for="fac">Faculty :</label>
					  <select id="fac" name="fac" required>
					    <option value="">Select course Faculty ..</option>
						<option value="Business">Business</option>
						<option value="Engineering">Engineering</option>
						<option value="pharmaceutical">pharmaceutical</option>
						<option value="Sport">Sport </option>
						<option value="Science">Science </option>
					  </select>	
					</li>
					<li>
						<label for="degree">Degree :</label>
					  <select id="degree" name="degree" required>
					    <option value="">Select Course Degree..</option>
						<option value="Associate degree">Associate degree</option>
						<option value="Bachelor degree">Bachelor degree</option>
						<option value="Master degree">Master degree	</option>
						<option value="Doctoral degree">Doctoral degree	</option>
					  </select>	
					</li>
					
					<li>
						<label for="course">Course :</label>
						<input type="text" id="course" class="text" name="course" placeholder="Enter your Course Name" required>
						
					</li>
					<li>
						<label for="course">Year :</label>
						<input type="text" id="year" class="text" name="year" placeholder="ie: 2018" required>
						
					</li>
					<li>
						<label for="att">Attendance :</label>
					    <input type="radio" name="att" value="Yes" class="att" onClick="document.getElementById('fee').value='45 €'" checked> Yes
					    <input type="radio" name="att" class="att" value="No" onClick="document.getElementById('fee').value='25 €'" > No<br>
					</li>
					<li>
						<label for="fee">Fees :</label>
						<input type="text" id="fee" class="text" placeholder="Fees required" readonly>
						
					</li>
					<li>
						<label for="submit">&nbsp;</label>
						<input type="submit" id="submit" name="submit" value="Submit">
					</li>
				</ul>
			</form>	
		</div>	
		</div>
	</body>
	<footer>
		<div class="foot">
		<p>Get social with us..
		<a href="https://twitter.com/AthloneIT?lang=en"><img src="t.gif" class="foo"></a>
		<a href="https://www.facebook.com/athloneit/"><img src="f.gif" class="foo"></a>
		<a href="https://www.linkedin.com/school/860289/"><img src="i.gif" class="foo"></a></p>
		
		</div>
	</footer>

</html>