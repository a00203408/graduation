body {
	font: "Lucida Sans", "Lucida Grande", "Lucida Sans Unicode", sans-serif;
	background-repeat: no-repeat;
    background-attachment: fixed;
	background-position:center;
	background-size: cover;
	
	}


ul{
	list-style-type:none;
	/* border-top: 2px solid #333; */
	/* border-bottom: 2px solid #333; */
}

label{
	display:block;
	width:10em;
	float:left;
	vertical-align:bottom;
	font-style:bold;
}

li{
	padding-top:.5em;
	padding-bottom: .5em;
}


input.text{
	padding:.5em;
	border:1px solid #BBB;
	border-radius:4px;
	box-shadow: 0px 0px 3px #ccc, 0 10px 20px #eee inset;
	padding-right:3em;
	transition: padding-right 1s;
}

input:focus{
	padding-right:5em;
}

input[type="submit"]{
	padding:.5em;
	border:1px solid #BBB;
	border-radius:4px;
	width:8em;
	background:darkorange;
	color:white;
	font-weight:bold;
}

span{
	font-size:90%;
	color:#AAA;
}

input:valid:required{
	box-shadow: 0 0 5px green;
	border-color: green;
}

input:focus:invalid:required{
	box-shadow: 0 0 5px red;
	border-color:red;
}

#gifs {
	opacity: 0.1;
	repeat:no-repeat;
	position:fixed;
	size:cover;
	padding-top:1px;
}

select {
	padding:.5em;
	border:1px solid #BBB;
	border-radius:4px;
	box-shadow: 0px 0px 3px #ccc, 0 10px 20px #eee inset;
	padding-right:3em;
	transition: padding-right 1s;
}

select:focus{
	padding-right:5em;
}


select:valid:required{
	box-shadow: 0 0 5px green;
	border-color: green;
}

select:focus:invalid:required{
	box-shadow: 0 0 5px red;
	border-color:red;
}



.sm {
    margin-top:10px;
	margin-left:30%;
    color:black;
    padding:20px;
    border:2px solid black;
    background:rgba(251, 229, 190, 0.6);
	width:500px;
	height:250px;
	float:center;
	
}

#logo {
	border-radius:50%;
	border:2px solid black;
	margin-left:35%;
	height: 150px;
    width: 150px;
}

#logos {
	border-radius:50%;
	border:2px solid black;
	margin-left:10%;
	height: 100px;
    width: 100px;
}

form h2{
	text-align:center;
}
#inf {
	text-align:right;
	display:initial;
}


.nav {
    float: left;

}

.nav a, .dropbtn {
    display: block;
    color: white;
    text-align: center;
    padding: 6px 16px;
    text-decoration: none;
}

.nav a:hover, .dropdown:hover .dropbtn { 
    background-color: #9b8a7d;
}
li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #dbc0a7;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: white;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color: #a69385}

.dropdown:hover .dropdown-content {
    display: block;
}

.foot{
 position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   height:30px;
   background-color: #333;
   color: white;
   text-align: center;
   padding-top:1px;
   float:top;
}

footer p {
    margin-top: -1px;
   
}

.foot2{
 position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   height:100px;
   background-color: #222222;
   color: white;
   text-align: center;
   padding-top:1px;
   float:top;
   
}
.fo {
	text-align:left;
	text-indent:90px;
}
.foo {
	border-radius:50%;
	height:30px;
	width:30px;	
}

#ait{
	width:90px;
	height:50px;
}

#map {position: fixed;padding-bottom: 20%;padding-top: 28px;margin-top:30px;width: 550px;height: 180px;float:center;margin-left:5%;}

#mapHeading {
	font-size: 300%;
	color: #404040;
	margin: 0;
	padding: 0;
	text-align: center;
}

#cu {
	border-bottom:2px solid black;
}

#sc{    margin: initial;
    color: cornsilk;
	font-style: oblique;
	font-family: "Times New Roman", Times, serif;}


table {
	margin: 0;
	background: linear-gradient(45deg, #c30dc440, #2c6182ba);
	font-family: "Times New Roman", Times, serif;
	font-weight: 100;
	border:2px solid black;
	border-style: ridge;
	border-radius:6px;
    width: -webkit-fill-available;
}


table.wide-table { width:100%; }

h5 {
	font-size:small;
    margin: initial;
    color: rgba(251, 229, 190, 0.9);
	font-family: "Times New Roman", Times, serif;}
#data {
	font-size:small;
	font-family: "Times New Roman", Times, serif;
}

.line {
	border: 1px solid black;
    width: 500px;
	align:center;
}
