#!/usr/bin/Rscript --options-you-need

library(C50)
library(gmodels)
library(rpart)
library(rpart.plot)
library(tidyverse)

library(ggplot2)
library(plotly)
library(gapminder)
library(ggpubr)
library(viridis)

library(RMySQL)
library(dbConnect)
library(sqldf)
library(partykit)
library(party)

install.packages("colorspace")

#Student<- read.csv("students.csv" , sep = ";" , stringsAsFactors=FALSE)
#str(Student)
#Student<- read.csv("studentAlaa.csv" , sep = ";" , stringsAsFactors=FALSE)
#str(Student)
con=dbConnect(MySQL(), user = 'root', password = '', dbname='students', host = 'localhost')
dbListTables(con)
Student <- dbReadTable(con, 'students')
rows <- nrow(Student)




Student$year <- as.Date(Student$year)
Student$attend <- as.factor(Student$attend)
Student$year <- as.factor(Student$year)
Student$course <- as.factor(Student$course)
Student$faculty <- as.factor(Student$faculty)
Student$degree <- as.factor(Student$degree)
str(Student)
#Student <- Student[-2]
Student
dim(Student)
summary(Student)


theme_set(theme_bw())

# Plot
jpeg("lollipop.jpg")
ggplot(Student, aes(x=Student$degree, y=Student$course)) + 
  geom_point(size=3) + 
  geom_segment(aes(x=Student$degree, 
                   xend=Student$degree, 
                   y=0, 
                   yend=Student$course)) + 
  labs(title="Degrees registered for different courses", 
       subtitle="the degrees registered for different courses ", 
       caption="source: Alaa",
       x = "Degrees Registered",
       y = "Courses Registered") + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6))
dev.off()

jpeg("degrees.jpg")
g <- ggplot(Student, aes(Student$degree))
g + geom_bar(aes(fill=Student$degree), width = 0.5) + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6)) + 
  labs(title="Number of students per Degree", 
       x = "Degrees Registered",
       y = "Number of students",
       subtitle="overall number of registered students based on thier degree") 
dev.off()



# From on a categorical column variable
jpeg("courses.jpg")
g <- ggplot(Student, aes(Student$faculty))
g + geom_bar(aes(fill=Student$course), width = 0.3) + 
  theme(axis.text.x = element_text(angle=65, vjust=0.6)) +
  labs(title="Courses in each Faculty per number of students", 
       subtitle="number of students per registered courses in each Faculty",
       x = "Faculty Registered",
       y = "Number of students",
       caption="Source:Alaa")
dev.off()

input.dat <- readingSkills[c(1:719),]
output.tree <- ctree(
  Student$year ~ Student$attend, 
  data = Student)
jpeg("years.jpg")
plot(output.tree)
dev.off()

input.dat <- readingSkills[c(1:719),]
output.tree2 <- ctree(
  Student$faculty ~ Student$attend,
  data = Student)
jpeg("tree2.jpg")
plot(output.tree2)
dev.off()

input.dat <- readingSkills[c(1:719),]
output.tree3 <- ctree(
  Student$attend ~ Student$faculty, Student$course,
  data = Student)
jpeg("plot.jpg")
plot(output.tree3)
dev.off()

jpeg("facu.jpg")
ggplot(Student, aes(Student$year, fill=Student$faculty ) ) +
  geom_bar(position="dodge")  +
  scale_fill_grey(start = 0, end = 1)+
  labs(title= "the variation of numbers per faculty each year", 
       x = "Years",
       y = "Number of students",
       caption="Source:Alaa")
dev.off()


