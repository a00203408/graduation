function myMap() {
var mapOptions = { 
    center: new google.maps.LatLng(53.418783, -7.903937),
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.HYBRID
}
var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}