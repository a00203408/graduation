
<html lang="en">
	<head>
		<title>Graduates Registration</title>
		<meta charset="UTF-8">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="insta.js"></script>
		<link rel="stylesheet" href="home.css">
			<link href='https://fonts.googleapis.com/css?family=Cinzel Decorative' rel='stylesheet'>
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="20152.jpg" alt="Grads1" width="1200" height="700">
        <div class="carousel-caption">
          <h3>Graduation 2015</h3>
          <p>“What we learn with pleasure we never forget.” —Alfred Mercier</p>
        </div>      
      </div>

      <div class="item">
        <img src="2016.jpg" alt="Grads2" width="1200" height="700">
        <div class="carousel-caption">
          <h3>Graduation 2016</h3>
          <p>“An investment in knowledge always pays the best interest.” —Benjamin Franklin</p>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">AIT Graduation</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
	  
			<li class="nav"><a href="https://www.ait.ie">AIT Home page</a></li>
			<li class="nav"><a href="http://timetable.ait.ie/students.htm">Student Timetable</a></li>
			<li class="dropdown">
			    <a href="https://www.ait.ie/life-at-ait/registry/graduation" class="dropbtn">Graduation</a>
				<div class="dropdown-content">
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Timetable</a>
					<a href="https://www.lafayette.ie ">Graduation Photography</a>
					<a href="https://www.phelanconan.com/">Academic Dress</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Graduation Ball</a>
					<a href="https://www.ait.ie/life-at-ait/registry/graduation">Assembly Point</a>		
			<li class="nav"><a href="location.html">Graduation Location</a></li>
			<li class="nav"><a href="contact.html">Contact us</a></li>
		</ul>
		</div>
		</div>
		</nav>
			<!--<body background="back.gif"/>-->
			<body background="new2.jpg"/>
		<div class="s">
		
			<img src="aitt.png" id="gifs"/>

			<p><strong>Welcome to AIT Graduates Home page. This website is dedicated to the Graduation ceremony and all information related to it. you can login down below as either student or as a member of our staff </strong></p>
			
			<button onclick="location.href='index.php'" class="button" style="vertical-align:middle" id="sl"><span>Student Login </span></button>
			<button onclick="location.href='indexx.html'" class="button" style="vertical-align:middle" id="st"><span>Staff Login </span></button>	
			<div>
				<img src="award.gif" id="award"/>
				<img src="good.png" id="oo"/><video width="450" autoplay controls>
				<source src="ait.mp4" type="video/mp4"></video>
			</div>
		</div>	
<main>


<!-- InstaWidget -->
<a href="https://instawidget.net/v/tag/athloneit" id="link-605ac398b9d58dde3e8e11b7d2b72236db94f9e360735bc25b8eeee09cd31b72">#athloneit</a>
<script src="https://instawidget.net/js/instawidget.js?u=605ac398b9d58dde3e8e11b7d2b72236db94f9e360735bc25b8eeee09cd31b72&width=120px"></script>		
<!-- 
<!-- InstaWidget 
<a href="https://instawidget.net/v/tag/athloneit" id="link-c7f68298bf96a721daaa8daaf23dfa73889c3137f9affb4b2292f41007acf0a0">#athloneit</a>
<script src="https://instawidget.net/js/instawidget.js?u=c7f68298bf96a721daaa8daaf23dfa73889c3137f9affb4b2292f41007acf0a0&width=120px"></script>

InstaWidget 
<a href="https://instawidget.net/v/user/athloneit" id="link-25bdac49369c4b7d47bfae2c10b69776fe611579a976bb44e86119a142a635e3">@athloneit</a>
<script src="https://instawidget.net/js/instawidget.js?u=25bdac49369c4b7d47bfae2c10b69776fe611579a976bb44e86119a142a635e3&width=160px"></script>
 InstaWidget 
<a href="https://instawidget.net/v/user/athloneit" id="link-aac554d48def50f1d38eeda00b48956b96e3e6054bfa2cda11c634e61200943e" class="inst">@athloneit</a>
<script src="https://instawidget.net/js/instawidget.js?u=aac554d48def50f1d38eeda00b48956b96e3e6054bfa2cda11c634e61200943e&width=160px"></script>
-->
<!--<iframe style="border:none" src="https://s3.amazonaws.com/files.photosnack.com/iframejs/widget.html?hash=pdx36illt&t=1515125499" width="100%" height="100%" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
-->
<ul id="insta"></ul>


 
</main>
</body>

	<footer>
		<div class="foot">
		<p>Get social with us..
		<a href="https://twitter.com/AthloneIT?lang=en"><img src="t.gif" class="foo"></a>
		<a href="https://www.facebook.com/athloneit/"><img src="f.gif" class="foo"></a>
		<a href="https://www.linkedin.com/school/860289/"><img src="i.gif" class="foo"></a></p>
		
		</div>
	</footer>

</html>

