package com.example.jilla.graduationapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by jilla on 23/02/2018.
 */

public class Payment extends AppCompatActivity {

    Button Pay;
    EditText CardNumber, ExpDate, cvc;
    CheckBox Yes, No;
    String cardNumberHolder;
    String expDateHolder;
    String cvcHolder;
    Boolean CheckEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        //Assign Id'S
        CardNumber = (EditText) findViewById(R.id.CardNumber);
        ExpDate = (EditText) findViewById(R.id.ExpDate);
        cvc = (EditText) findViewById(R.id.cvc);
        Yes = (CheckBox) findViewById(R.id.Yes);
        No = (CheckBox) findViewById(R.id.No);

        Pay = (Button) findViewById(R.id.Pay);

        //Adding Click Listener on button.
        Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Checks();

                if (CheckEditText) {

                    Intent intent = new Intent(Payment.this, DashboardActivity.class);
                    startActivity(intent);

                    // Checking whether EditText is Empty or Not


                    // If EditText is not empty and CheckEditText = True then this block will execute.

                    //PaymentsDetails(cardNumberHolder, expDateHolder, cvcHolder, yesHolder, noHolder);

                } else {

                    // If EditText is empty then this block will execute .
                    Toast.makeText(Payment.this, "Please fill all fields and authorize this payment.", Toast.LENGTH_LONG).show();

                }


            }
        });}


    public void Checks() {

        cardNumberHolder = CardNumber.getText().toString();
        expDateHolder = ExpDate.getText().toString();
        cvcHolder = cvc.getText().toString();



        if (TextUtils.isEmpty(cardNumberHolder) || cardNumberHolder.length() != 16 || TextUtils.isEmpty(expDateHolder)
                || expDateHolder.length() != 4 || TextUtils.isEmpty(cvcHolder) || cvcHolder.length() != 3 ||No.isSelected()==true ) {
            Yes.setSelected(false);
            CheckEditText = false;

        } else {
            No.setSelected(false);
            CheckEditText = true;
        }

    }
}