package com.example.jilla.graduationapp;
/**
 * Created by jilla on 22/02/2018.
 */
        import android.app.ProgressDialog;
        import android.content.Intent;
        import android.os.AsyncTask;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.text.TextUtils;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;
        import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    EditText StudentId, Password;
    Button LogIn ;
    String StudentIdHolder, PasswordHolder ;

    String Details ;
    String HttpURL = "http://10.0.2.2:8050/last/Login.php";
     //String HttpURL = "http://192.168.0.38:8070/last/Login.php";
    //String HttpURL = "http://193.1.30.2:8060/last/Login.php";
    //String HttpURL = "http://92.168.43.128:8060/last/Login.php";

    Boolean CheckEditText ;
    ProgressDialog progressDialog;
    HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String Student = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlogin);

        StudentId = (EditText)findViewById(R.id.studentId);
        Password = (EditText)findViewById(R.id.password);
        LogIn = (Button)findViewById(R.id.Login);

        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EmptyCheck();

                if(CheckEditText){

                    UserLogin(StudentIdHolder, PasswordHolder);

                }
                else {

                    Toast.makeText(LoginActivity.this, "Please check your details", Toast.LENGTH_LONG).show();

                }

            }
        });
    }
    public void EmptyCheck(){

        StudentIdHolder = StudentId.getText().toString();
        PasswordHolder = Password.getText().toString();

        if(TextUtils.isEmpty(StudentIdHolder) || TextUtils.isEmpty(PasswordHolder))
        {

            CheckEditText = false;
        }
        else {

            CheckEditText = true ;
        }
    }

    public void UserLogin(final String username, final String password){

        class LoginClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(LoginActivity.this,"Loading....",null,true,true);
            }

            @Override
            protected void onPostExecute(String httpResponseMsg) {

                super.onPostExecute(httpResponseMsg);

                progressDialog.dismiss();

                if(httpResponseMsg.equalsIgnoreCase("Data Matched")){

                    finish();

                    Intent intent = new Intent(LoginActivity.this, Register.class);

                    intent.putExtra(Student,username);

                    startActivity(intent);

                }
                else{

                    Toast.makeText(LoginActivity.this,httpResponseMsg,Toast.LENGTH_LONG).show();
                }

            }

            @Override
            protected String doInBackground(String... params) {

                hashMap.put("username",params[0]);

                hashMap.put("password",params[1]);

                Details = httpParse.postRequest(hashMap, HttpURL);

                return Details;
            }
        }

        LoginClass LoginClass = new LoginClass();

        LoginClass.execute(username,password);
    }

}

