package com.example.jilla.graduationapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by jilla on 23/02/2018.
 */

public class Register extends AppCompatActivity {

    Button Pays;
    EditText Faculty, Degree, Name, Course;
    TextView Pay;
    RadioGroup Group;
    RadioButton Yes, No;
    String FacultyHolder, DegreeHolder, NameHolder, CourseHolder, value;
    String finalResult ;
    String HttpURL = "http://10.0.2.2:8050/last/Registration.php";
    //String HttpURL = "http://192.168.0.38:8070/last/Registration.php";
   // String HttpURL = "http://193.1.30.2:8060/last/Registration.php";
   // String HttpURL = "http://193.1.30.2:8060/last/Registration.php";
    //String HttpURL = "http://192.168.43.128:8060/last/Registration.php";

    Boolean CheckEditText ;
    ProgressDialog progressDialog;
    HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String student = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Assign Id'S
        Faculty = (EditText) findViewById(R.id.Faculty);
        Degree = (EditText) findViewById(R.id.Degree);
        Name = (EditText) findViewById(R.id.Name);
        Course = (EditText) findViewById(R.id.Course);
        Pay = (TextView) findViewById(R.id.Pay);
        Group = (RadioGroup)findViewById(R.id.Group);
        Pays = (Button) findViewById(R.id.Pays);

        Group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.Yes:

                        Pay.setText("45 €");

                        break;
                    case R.id.No:

                        Pay.setText("25 €");
                        break;
                }
            }
        });

        //Adding Click Listener on button.
        Pays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Checking whether EditText is Empty or Not
                CheckEmpty();

                if (CheckEditText) {
                    UserRegister(FacultyHolder, DegreeHolder, NameHolder, CourseHolder,value);


                } else {

                    // If EditText is empty then this block will execute .
                    Toast.makeText(Register.this, "Please fill Registration fields.", Toast.LENGTH_LONG).show();

                }


            }
        });}

    public void CheckEmpty() {

        FacultyHolder = Faculty.getText().toString();
        DegreeHolder = Degree.getText().toString();
        NameHolder = Name.getText().toString();
        CourseHolder = Course.getText().toString();
        value = ((RadioButton)findViewById(Group.getCheckedRadioButtonId())).getText().toString();



        if (TextUtils.isEmpty(FacultyHolder) || TextUtils.isEmpty(DegreeHolder) || TextUtils.isEmpty(NameHolder) || TextUtils.isEmpty(CourseHolder)) {

            CheckEditText = false;

        } else {

            CheckEditText = true;
        }

    }

    public void UserRegister(final String name, final String fac, final String degree, final String course, final String att){
        //attend = value;
        class UserRegisterClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(Register.this, "Loading Data", null, true, true);
            }

            @Override
            protected void onPostExecute(String httpResponseMsg) {

                super.onPostExecute(httpResponseMsg);

                progressDialog.dismiss();

                //Toast.makeText(Register.this,httpResponseMsg.toString(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Register.this,Payment.class);
                startActivity(intent);

            }

            @Override
            protected String doInBackground(String... params) {


                hashMap.put("fac", params[0]);

                hashMap.put("degree", params[1]);

                hashMap.put("name", params[2]);

                hashMap.put("course", params[3]);

                hashMap.put("att", params[4]);

                finalResult = httpParse.postRequest(hashMap, HttpURL);

                return finalResult;
            }
        }

        UserRegisterClass userRegisterClass = new UserRegisterClass();

        userRegisterClass.execute(name,fac,degree,course,att);
        }
}

